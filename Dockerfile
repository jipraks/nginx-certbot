FROM nginx:1.18

RUN apt update -y
RUN apt install python3-acme python3-certbot python3-mock python3-openssl python3-pkg-resources python3-pyparsing python3-zope.interface -y
RUN apt install python3-certbot-nginx -y